package com.tucans.adsserviceadvisor;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

public class DataComm extends AsyncTask<String, Void, JSONObject> {

	GetJSON activity;
	private String baseURL = "http://10.0.0.3:4567/ADSService.svc/";//?q=p_services/system/";
	static DefaultHttpClient httpClient = null;
	String errorMessage = "No Error";

	public DataComm(GetJSON _activity) {
		activity = _activity;
	}

	@Override
	protected void onPreExecute() {
		activity.jsonStart();
	}

	@Override
	protected JSONObject doInBackground(String... params) {
		HttpResponse response;
		JSONObject jsonResponse = null;
		try {
			if (httpClient == null){
				httpClient = new DefaultHttpClient();
			}
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000); //Timeout Limit
			HttpGet request = new HttpGet(baseURL + params[0]);
		    AndroidHttpClient client = AndroidHttpClient.newInstance("Android"); 
		    response = client.execute(request);
			String responseStr = EntityUtils.toString(response.getEntity());
			jsonResponse = new JSONObject(responseStr);

		} catch (ClientProtocolException e) {
			if (e.getMessage() != null)
				errorMessage = e.getMessage();
			response = null;
			e.printStackTrace();
		} catch (IOException e) {
			if (e.getMessage() != null)
				errorMessage = e.getMessage();
			e.printStackTrace();
			response = null;
		} catch (Exception e) {
			if (e.getMessage() != null)
				errorMessage = e.getMessage();
			response = null;
		} finally {

		}
		return jsonResponse;
	}

}
